import numpy as np
import math

#from silence_tensorflow import silence_tensorflow
#silence_tensorflow()

import tensorflow as tf
from tensorflow.keras import layers, Model, Sequential
from tensorflow.keras.layers import Dense, Input, BatchNormalization, LSTM, Dropout
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.metrics import log_loss   # Cross entropy

import keras.backend as K        # Todo remove?

class RNN:
    def __init__(self, input_size, time_steps):
        """
        Use gradient tape to interact directly with gradients during training?s
        with tf.GradientTape(): etc....
        """
        # Creating the RNN network
        self.rnn = Sequential()

        #self.rnn.add(LSTM(units=10, input_shape=(batch_size, input_size)))      # Parameter size here (units = 4?)
        self.rnn.add(LSTM(units=256,
                          recurrent_dropout=0.2,
                          input_shape=(time_steps, input_size)))      # Parameter size here (units = 4?)
        self.rnn.add(Dense(units=1, activation='linear'))      # Output layer (have just one output instead? Let the target be [timestep-1] (last sequences answer)

        #self.rnn.compile(loss='mean_squared_error', optimizer=tf.keras.optimizers.Adam(lr=0.002), metrics=['mse'])   # Compile model
        #self.rnn.compile(loss='mae', optimizer=tf.keras.optimizers.Adam(learning_rate=0.00005), metrics=['mae'])   # Compile model, mae loss
        self.rnn.compile(loss='mae', optimizer=tf.keras.optimizers.Adam(learning_rate=0.00005), metrics=['mae'])   # Compile model, mae loss

        self.rnn.summary()

    def train(self, train_X, train_y, test_X, test_Y, epochs=50):
        my_lr_scheduler = tf.keras.callbacks.LearningRateScheduler(self.adapt_learning_rate)
        history = self.rnn.fit(train_X, train_y, epochs=epochs, batch_size=128, shuffle=False, validation_data=[test_X, test_Y], callbacks=[my_lr_scheduler])
        return history

    def train_tf_dataset(self, train, test, epochs=50):
        history = self.rnn.fit(train, validation_data=test, epochs=epochs, batch_size=32, shuffle=False)
        return history

    def predict(self, X_test):
        return self.rnn(X_test)

    def save_params(self, file_name="model/nin1out"):
        self.rnn.save(file_name)

    def load_params(self, file_name="model/nin1out"):
        self.rnn = tf.keras.models.load_model(file_name)

    def adapt_learning_rate(self, epoch):
        lr = 0.0005 * 0.1**epoch
        print("Learning rate: ", lr)
        return lr

