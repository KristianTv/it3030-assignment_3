import copy

import pandas as pd
from pandas import concat
import matplotlib.pyplot as plt
import sys
import numpy as np
from tensorflow.keras.preprocessing import timeseries_dataset_from_array
from sklearn.model_selection import train_test_split
from scipy.interpolate import UnivariateSpline, PchipInterpolator, CubicSpline
from sklearn.preprocessing import MinMaxScaler, RobustScaler
import os
import math
import datetime
import csaps
from scipy.interpolate import interp1d

np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 1000)


class DataProcessing:
    # TODO Adjust for seasonality https://machinelearningmastery.com/time-series-seasonality-with-python/
    # TODO Transform Y and wind? Atleast y by interpolating and subtracting

    def __init__(self):
        self.dataframe = pd.read_csv("data/no1_train.csv")
        self.dataframe_val = pd.read_csv("data/no1_validation.csv")
        plt.plot(self.dataframe.iloc[2000:2040]['y'], label="Before interp")
        plt.legend()
        plt.show()

    def print_all(self):
        print(self.dataframe)

    def show_figures(self):
        print(self.dataframe.keys())

        # plt.plot(self.dataframe['hydro'])
        # plt.show()
        # plt.plot(self.dataframe['micro'])
        # plt.show()
        # plt.plot(self.dataframe['thermal'])     # Just a noisy parameter? Is there even any info here
        # plt.show()
        # plt.plot(self.dataframe['wind'])        # Contains anomalies, looks quite noisy, but may be of use if processed
        # plt.show()
        # plt.plot(self.dataframe['total'])
        # plt.show()
        # plt.plot(self.dataframe['y'])
        # plt.show()
        # plt.plot(self.dataframe['sys_reg'])
        # plt.show()
        # plt.plot(self.dataframe['flow'])
        # plt.show()

    def feature_engineering(self, df):
        # TODO Add season and leap year? 2020 was a leap year
        # TODO SEASON ATLEAST!
        # Season: Spring (March, April, May) (3, 4, 5)
        #         Summer (June, July, August) (6, 7, 8)
        #         Autumn (September, October, November) (9, 10, 11)
        #         Winter (December, January, February) (12, 1, 2)

        df['start_time'] = pd.to_datetime(df['start_time'])  # Converting to timestamp
        date_series = df['start_time']

        year = date_series.dt.year  # Year
        month = date_series.dt.month  # Month of the year
        day = date_series.dt.day  # Day of the month
        print(len(month))
        print(len(df['start_time']))
        #df.loc['winter'] = 0
        #df.loc['spring'] = 0
        #df.loc['summer'] = 0
        #df.loc['autumn'] = 0

        #df.loc[month.isin([12, 1, 2])]['winter'] = 1
        #df.loc[month.isin([3, 4, 5])]['spring'] = 1
        #df.loc[month.isin([6, 7, 8])]['summer'] = 1
        #df.loc[month.isin([9, 10, 11])]['autumn'] = 1

        #print(df.loc[month.isin([9, 10, 11])]['autumn'])

        week = date_series.dt.isocalendar().week  # Week of the year
        weekday = date_series.dt.isocalendar().day  # Day of the week
        hour = date_series.dt.hour  # Current hour
        minute = date_series.dt.hour * 60 + date_series.dt.minute  # Total minutes of day passed


        # Add the most important time attributes to the dataframe:
        df.insert(loc=0, column='week', value=week)
        df.insert(loc=0, column='weekday', value=weekday)
        df.insert(loc=0, column='hour', value=hour)
        df.pop('start_time')  # Remove the start time from dataset

        return df


    def add_lagged_target(self, df):
        df['previous_y'] = df['y'].shift()  # Creates a shifted version
        return df

    def fetch_interpolated_target(self):    # TODO Not currently used
        # Shift, add then divide by two to get midpoints ye
        y = copy.deepcopy(self.dataframe['y'])
        #interpolated_y = (y + y.shift(1)+y.shift(2)+y.shift(3)+y.shift(4)+y.shift(5)+y.shift(6)+y.shift(7)) / 8  # Creating midpoints - average between two points
        interpolated_y = (y + y.shift(1)) / 2  # Creating midpoints - average between two points
        self.dataframe['y'] = y - interpolated_y

    def remove_structural_imbalance(self, df, visualize=True):
        planned_flow = df['flow']
        planned_production = df['total']

        # Sum all the planned flow AND production
        summed = planned_flow + planned_production

        # Every 6th entry being every 30 minutes
        summed_midpoints = summed.copy()    #
        summed_midpoints.loc[:] = np.nan
        summed_midpoints.loc[::6] = summed[::6]

        interpolated = summed_midpoints.interpolate(method='cubic')

        print("Interpolated: ", len(interpolated))
        # Subtract this sum from the same sum just interpolated
        structure_imbalance = interpolated - summed

        if visualize:
            # Illustrating the interpolation
            plt.plot(interpolated[0:100])
            plt.scatter(np.arange(0, 100), summed_midpoints[0:100])
            plt.show()

        # Subtract this from the target (y - shiz)
        # TODO This should be an input feature not replace or "fix" target
        # TODO Maybe otherwise check if the new target or feature performs better
        df['demand_curve'] = df['y'] - structure_imbalance
        return df

    def fetch_data(self, test=False):
        return self.dataframe_val if test else self.dataframe

    def fetch_train_test(self, df, test_size=0.33):
        y = df.pop('y')  # Extracting y
        x = df  # Extracting x
        return train_test_split(x, y, test_size=test_size, shuffle=False)

    def normalize(self, column_to_scale):
        """
        Takes a dataframe column as parameter, scales it using MinMaxScaler and returns scaled column
        :param column_to_scale: Column to scale
        :return: Dataframe column
        """
        print(column_to_scale.to_numpy().shape)
        scaler = RobustScaler()
        scaled_column = scaler.fit_transform(column_to_scale.to_numpy().reshape(-1, 1))
        return pd.DataFrame(scaled_column)
        #column_to_scale = column_to_scale.values()
        #min_max_scaler = MinMaxScaler()
        #return pd.DataFrame(min_max_scaler.fit_transform(column_to_scale))

    def standardize(self, column_to_scale):
        """
        Takes a dataframe column as parameter, standardizes it using X and returns scaled column
        :param column_to_scale: Column to scale
        :return: Dataframe column
        """
        return column_to_scale
        #column_to_scale = column_to_scale.values()
        #min_max_scaler = MinMaxScaler()
        #return pd.Dataframe(min_max_scaler.fit_transform(column_to_scale))

    def offset_24hours(self, df):
        df['y_24prev'] = df['y'].shift(288)  # Offset 24 hours (24*60 = 1440. 1440 / 5 = 288)
        return df

    def clamp_column(self, df_column):
        """
        Clipping values (less than 1% of the dataset)
        :return: Clipped column
        """
        higher_quantile = df_column.quantile(0.995)
        lower_quantile = df_column.quantile(0.005)
        print("Higher Q:", higher_quantile)
        print("Lower Q:", lower_quantile)
        n_clipped = len(df_column[df_column >= higher_quantile]) + len(df_column[df_column <= lower_quantile])
        percentage = n_clipped / len(df_column) * 100
        print("Percent clipped: ", percentage, "%")
        return df_column.clip(lower=lower_quantile, upper=higher_quantile)

    def convert_to_lstmdata(self, data, n_in=1, n_out=1):
        """

        :param data: The cleaned dataframe
        :param n_in: Number of timesteps for one input
        :param n_out: Number of timesteps into the future to predict for one input sequence
        :return:
        """
        n_features = data.shape[1]  # [samples, features]
        df = pd.DataFrame(data)  # Makes sure it is a dataframe

        cols, names = list(), list()  # Lists to save columns and names?

        # input sequence (the range of input before the prediction)
        for i in range(n_in, 0, -1):  # Reversing
            cols.append(df.shift(i))  # Adding all the shifted features to one sequence
            names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_features)]  # Naming the features

        # Forecasting sequence (the ones to be predicted
        for i in range(0, n_out):
            cols.append(df.shift(-i))

            if i == 0:  # (t) Naming the features
                names += [('var%d(t)' % (j + 1)) for j in range(n_features)]
            else:  # (t+n)
                names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_features)]

        # Create a complete trainable LSTM sequence with n inputs and m steps to be predicted
        training_sample = concat(cols, axis=1)  # Multiple dataframes -> one continuous dataframe
        training_sample.columns = names  # Setting correct column names/labels (with timeshift info)

        return training_sample

    def convert_to_lstmdata_keras(self, data, n_in=1, n_out=1):
        """

        :param data: The cleaned dataframe
        :param n_in: Number of timesteps for one input
        :param n_out: Number of timesteps into the future to predict for one input sequence
        :return:
        """
        n_features = data.shape[1]  # [samples, features]
        df = pd.DataFrame(data)  # Makes sure it is a dataframe
        y = np.asarray(df['y']).astype(np.float32)[n_in:]
        x = np.asarray(df.drop(columns=['y'])).astype(np.float32)[:-n_in]

        training_sample = timeseries_dataset_from_array(data=x,
                                                        targets=y,
                                                        sequence_length=n_in,
                                                        batch_size=128,
                                                        start_index=300,    # Start offset
                                                        shuffle=True)

        return training_sample

    def clean_data(self, df):
        """
        Notes:
        Hydro - Looks OK
        Micro - Looks OK
        Thermal - Contains 0    TODO Check further - (Filled in obvious)
        Wind - Contains 0   TODO Check further - (Filled in obvious)
        Total - Looks OK
        Sys_reg - Loads of 0    TODO Check further
        Flow  -  Looks OK (All negative almost?)  TODO Check further

        Look to see if hydro+micro+thermal+wind adds up to total [DONE]

        :return:
        """

        # Fix the wrong negation in flow feature
        df["flow"] = -df["flow"]

        # Column river is just 0 (remove)
        df = df.drop(columns=['river'], axis=0)

        # 55 980 Where total is not right. Check for missing entries for those that does not match
        total_time_sum = df['hydro'] + df['micro'] + df['thermal'] + df[
            'wind']

        faulty = df[df['total'] != total_time_sum]

        missing_wind_indexes = faulty[faulty['wind'] == 0].index  # Extract indexes of culprits
        missing_thermal_indexes = faulty[faulty['thermal'] == 0].index  # Extract indexes of culprits

        df.loc[missing_wind_indexes, 'wind'] = \
            (df.iloc[missing_wind_indexes]['total'] -
             df.iloc[missing_wind_indexes]['hydro'] +
             df.iloc[missing_wind_indexes]['micro'] +
             df.iloc[missing_wind_indexes]['thermal'])

        # print(df.iloc[missing_wind_indexes]['wind'])   # Print to check newly created values

        df.loc[missing_thermal_indexes, 'thermal'] = \
            (df.iloc[missing_thermal_indexes]['total'] -
             df.iloc[missing_thermal_indexes]['hydro'] +
             df.iloc[missing_thermal_indexes]['micro'] +
             df.iloc[missing_thermal_indexes]['wind'])


        #df['wind'] = np.sqrt(df['wind'])        # Transform the wind parameter with square root

        # TODO Check if thermal has any inpact
        # df.pop('thermal')
        # df.pop('wind')
        # print(df.loc[missing_thermal_indexes, 'thermal'])   # Print to check newly created values

        # print("Overlapping IDs in missing planned power: ", (faulty[faulty['thermal'] == 0].index.isin(faulty[faulty['wind'] == 0].index)).sum())

        # print(df.describe())
        return df


    def print_hist(self, column):
        plt.hist(column)
        plt.show()


