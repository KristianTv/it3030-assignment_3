from ImbalanceForecast import ImbalanceForecast

if __name__ == "__main__":
    load = True
    altered = False
    IF = ImbalanceForecast(load=load, altered=altered)
    if not load:
        #IF.multistep_training_tf_dataset(epochs=1)
        IF.multistep_training()
    IF.multistep_forecast()
