import copy

import matplotlib.pyplot as plt
from RNN import RNN
from DataProcessing import DataProcessing
import matplotlib.pyplot as plt
import numpy as np
from sklearn.utils import shuffle

class ImbalanceForecast:
    def __init__(self, load, altered):
        self.time_steps = 144          # The amount of timesteps as inpit including the output
        self.filename = "nin1out"
        self.data_processing = DataProcessing()

        self.train = self.data_processing.fetch_data(test=False)
        self.hold_out = self.data_processing.fetch_data(test=True)

        # Clipping Y to 0.005 and 0.995th percentile
        self.train['y'] = self.data_processing.clamp_column(self.train['y'])
        self.train = self.data_processing.offset_24hours(self.train)  # Creates a new target that is offset 24 hours prior.

        self.train['y'] = self.data_processing.normalize(self.train['y'])
        self.train['hydro'] = self.data_processing.normalize(self.train['hydro'])
        self.train['micro'] = self.data_processing.normalize(self.train['micro'])
        self.train['thermal'] = self.data_processing.normalize(self.train['thermal'])
        self.train['wind'] = self.data_processing.normalize(self.train['wind'])
        self.train['total'] = self.data_processing.normalize(self.train['total'])
        self.train['sys_reg'] = self.data_processing.normalize(self.train['sys_reg'])
        self.train['flow'] = self.data_processing.normalize(self.train['flow'])

        self.train = self.data_processing.feature_engineering(self.train)      # Add time features
        self.train = self.data_processing.clean_data(self.train)
        self.train = self.data_processing.add_lagged_target(self.train)

        # Clipping Y to 0.005 and 0.995th percentile
        self.hold_out['y'] = self.data_processing.clamp_column(self.hold_out['y'])
        self.hold_out = self.data_processing.offset_24hours(self.hold_out)  # Creates a new target that is offset 24 hours prior.

        self.hold_out['y'] = self.data_processing.normalize(self.hold_out['y'])
        self.hold_out['hydro'] = self.data_processing.normalize(self.hold_out['hydro'])
        self.hold_out['micro'] = self.data_processing.normalize(self.hold_out['micro'])
        self.hold_out['thermal'] = self.data_processing.normalize(self.hold_out['thermal'])
        self.hold_out['wind'] = self.data_processing.normalize(self.hold_out['wind'])
        self.hold_out['total'] = self.data_processing.normalize(self.hold_out['total'])
        self.hold_out['sys_reg'] = self.data_processing.normalize(self.hold_out['sys_reg'])
        self.hold_out['flow'] = self.data_processing.normalize(self.hold_out['flow'])

        self.hold_out = self.data_processing.feature_engineering(self.hold_out)  # Add time features
        self.hold_out = self.data_processing.clean_data(self.hold_out)
        self.hold_out = self.data_processing.add_lagged_target(self.hold_out)

        if altered:
            self.train = self.data_processing.remove_structural_imbalance(self.train)
            self.hold_out = self.data_processing.remove_structural_imbalance(self.hold_out)
            self.filename = "altered"

        self.X_train, self.X_test, self.y_train, self.y_test = self.data_processing.fetch_train_test(self.train, test_size=0.33)

        print("Train length: ", len(self.y_train))
        print("Test length:  ", len(self.y_test))
        print("Hold out length: ", len(self.hold_out))

        #self.y_train = self.train['y']
        #self.X_train = self.train.drop(columns=["y"])
        #self.y_test = self.train['y']
        #self.X_test = self.train.drop(columns=["y"])

        self.n_features = len(self.X_train.keys())+1       # Number of features and target  (without date and time)

        # Initialize the time series network with LSTM
        self.rnn = RNN(input_size=self.n_features-1, time_steps=self.time_steps)        # -1 because its without y in input

        if load:
            self.rnn.load_params(self.filename)

        self.train = copy.deepcopy(self.X_train)        # The entire set including Y
        self.train['y'] = self.y_train

        self.test = copy.deepcopy(self.X_test)  # The entire set including Y
        self.test['y'] = self.y_test

        self.test2 = copy.deepcopy(self.test)

    def multistep_forecast(self, history_len=60, n_preds=25):
        """
        n in, 1 out
        each input is n long, predicts next timestep.
        For each sequence, only the last one is the one that needs to be predicted. Which means 19 inputs and 1 output
        Which means that the previous_y is actually valid for the first prediction.
        BUT, for the proceeding ones, the (t+1) column should have previous_y set to previous one
        :return:
        """
        plt.plot(self.train.iloc[2000:2040]['y'], label="After interp")
        plt.legend()
        plt.show()

        # Generating test cases:
        # Making sure 'y' is last and 'previous_y' is second last
        self.hold_out['previous_y'], self.hold_out['y'] = self.hold_out.pop('previous_y'), self.hold_out.pop('y')

        samples_test = self.data_processing.convert_to_lstmdata(self.hold_out, n_in=self.time_steps-1, n_out=1)
        samples_test_original = samples_test.values.reshape((samples_test.shape[0], self.time_steps, self.n_features))

        offset = 5000            # Offset into the dataset to start predicting
        forecast_window_len = 24  # 2 Hours in 5 mins: 120/5 = 24
        padding = [np.nan]*history_len

        for n in range(n_preds):
            holdout = samples_test_original

            # TODO Show the true Y before as well
            # Only picking data from 300 and out to not include training data in 24 hours previous feature
            # Forecast_window_len*n to predict on separate "windows"
            # Offset to try different parts of the dataset
            # Copy here so the actual dataset is not changed
            offset_sum = 300 + self.time_steps+forecast_window_len*n+offset
            test_y = holdout[offset_sum-1:offset_sum+self.time_steps-1, :, -1]        # Pulling out Y
            test_x = holdout[offset_sum-1:offset_sum+self.time_steps-1, :, :-1]       # Sample data
            hist_y = holdout[offset_sum-history_len:offset_sum, :, -1]

            test_x = np.asarray(test_x).astype('float32')
            test_y = np.asarray(test_y).astype('float32')
            hist_y = np.asarray(hist_y).astype('float32')

            sequence_len = test_x.shape[1]
            features_len = test_x.shape[2]

            ############## Initializing first prediciton #######################
            forecasts = []                              # Buffer
            forecast = self.rnn.predict(test_x[0].reshape(1, self.time_steps, self.n_features-1))      # Predicting on first sequence to get previous_y
            forecasts.append(forecast[0].numpy())                  # Appending the forecast so it can be used to set previous_y later
            ####################################################################

            for pred_no in range(0, forecast_window_len-1):        # The distance into the future we are predicting!
                # [Next prediction, last sequence, last parameter] set to the last prediction
                # Prediction will output a estimate for every time step, we keep the last one only
                # Last feature is 'previous_y' after separating 'y'
                test_x[pred_no+1, sequence_len-1, features_len-1] = forecasts[pred_no]             # Setting "previous_y" of next sequence to last prediction
                forecast = self.rnn.predict(test_x[pred_no+1].reshape(1, self.time_steps, self.n_features-1))
                forecasts.append(forecast[0].numpy())   # Add the last prediction only

            x = np.arange(history_len, history_len+24)
            x_hist = np.arange(0, history_len)
            #combined = padding + forecast
            plt.plot(x_hist, hist_y[:, self.time_steps-1], label="Snippet of input history")  # TODO is this indexing right? Doesnt look like it
            plt.plot(x, forecasts, label="Forecast", color="red")
            #plt.plot(combined, label="Forecast", color="red")
            plt.plot(x, test_y[0:forecast_window_len, self.time_steps-1], label="Target")
            plt.legend()
            plt.show()

    def multistep_training(self):
        """
        Training on sliding windows? input of x size, pred and the target for the last indice
        :return:
        """
        # Extracting processed dataset to include n_in inputs (lagged variables) and n_outputs (of which to be predicted)
        samples = self.data_processing.convert_to_lstmdata(self.train, n_in=self.time_steps-1, n_out=1)
        samples_test = self.data_processing.convert_to_lstmdata(self.test, n_in=self.time_steps-1, n_out=1)

        # reshape input to be 3D [samples, timesteps, features] (turning into numpy array without labels)
        train_x = samples.values.reshape((samples.shape[0], self.time_steps, self.n_features))
        test_x = samples_test.values.reshape((samples_test.shape[0], self.time_steps, self.n_features))

        #print("Train shape: ", train_x.shape)
        #print(test_x[300+self.time_steps])
        # Starting training offset (atleast after 300(288)+timesteps) due to 24 hours previous parameter
        train_y = train_x[300+self.time_steps:, -1, -1]     # Use only last part of sequence as Y
        train_x = train_x[300+self.time_steps:, :, :-1]

        test_y = test_x[300+self.time_steps:15000, -1, -1]    # Use only last part of sequence as Y
        test_x = test_x[300+self.time_steps:15000, :, :-1]

        #print(test_y[0])
        #print(test_x[0])
        #print(self.test.iloc[self.time_steps:self.time_steps+self.time_steps])

        train_x = np.asarray(train_x).astype('float32')
        train_y = np.asarray(train_y).astype('float32')

        test_x = np.asarray(test_x).astype('float32')
        test_y = np.asarray(test_y).astype('float32')

        train_x, train_y = shuffle(train_x, train_y, random_state=0)    # Holy shit this worked extremely well

        history = self.rnn.train(train_x, train_y, test_x, test_y, epochs=5)
        self.rnn.save_params(self.filename)
        plt.plot(history.history['val_mae'], label="Val loss")
        plt.legend()
        plt.show()

    def multistep_training_tf_dataset(self, epochs=50):
        """
        Training on sliding windows? input of x size, pred and the target for the last indice
        :return:
        """
        # Extracting processed dataset to include n_in inputs (lagged variables) and n_outputs (of which to be predicted)
        samples = self.data_processing.convert_to_lstmdata_keras(self.train, n_in=self.time_steps-1, n_out=1)
        samples_test = self.data_processing.convert_to_lstmdata_keras(self.test, n_in=self.time_steps-1, n_out=1)

        history = self.rnn.train_tf_dataset(samples, samples_test, epochs=epochs)

        self.rnn.save_params(self.filename)
        plt.plot(history.history['val_mae'])
        plt.show()

